#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Analysis script for the plant cycle data set.

Author: Rik van Rosmalen
Contact: rik.vanrosmalen@wur.nl / rikpetervanrosmalen@gmail.com
"""
import collections
import itertools
import re
import os
import glob
import datetime
import functools
import numbers

import numpy as np
import scipy
import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import networkx as nx

from main import *


if __name__ == '__main__':
    # ------------------------------------------------------------------------------------------ #
    # Script settings
    # ------------------------------------------------------------------------------------------ #
    max_step = 15
    events_fill_na = True  # Forward fill NaN values to detect more events.
    time_resample_interval = '10T'  # 10 minutes
    resample_interpolate_function = lambda x: x.ffill()  # or .nearest / .bfill
    transition_interpolate = None  # 'ffill'
    counts_as_percentage = True
    add_start_end_hallmark = True  # Add the start and end hallmarks by default.
    n_permutations = 10000
    n_hallmark_stages = 12  # Amount of Hallmarks to add, excluding optional start/end.
    min_correlation_size = 5
    hallmark_plot_max_interval_times = ['60M', '0M', '30M', '60M', '1200M']

    dropcolumns = [
        # 'cell shape',
        # 'nucleus positoin',
        # 'nucleolus position',
        # 'REC8/Chromatin',
        # 'MT array',
        'Tapetum',
    ]
    heatmap_plot_order = ['cell shape', 'MT array', 'nucleus position',
                          'nucleolus position', 'REC8/Chromatin', 'Tapetum']

    # What should be plotted
    FIG_DPI = 300
    plot_series = False
    plot_hists = False
    plot_feature_chains = False
    plot_co_ocurrence = False
    plot_hallmark_scores = False # Note: Hallmark / landmark are used interchangeably.
    plot_hallmark_graph = False
    saveplots = False

    # Do the bootstrap analysis? (Takes some extra time.)
    bootstrap = False

    # Save results to tables?
    save_co_occurence_sheets = False
    save_event_info = False
    save_named_hallmark_info = False
    save_hallmark_sheets = False
    # Note that these results should not be misinterpreted!
    # There is a high correlation to the individuals samples here that is unaccounted for.
    save_state_duration_correlation = False
    save_hallmark_duration_correlation = False

    # Output files
    if saveplots:
        directories = [
            'results',
            'results/figures',
            'results/figures/co-occurences',
            'results/figures/hallmarks',
            'results/figures/timeseries',
            'results/figures/event_hist',
            'results/figures/chains',
        ]
        for d in directories:
            try:
                os.mkdir(d)
            except FileExistsError:
                pass

    if any([save_co_occurence_sheets, save_hallmark_sheets, save_state_duration_correlation,
            save_event_info, save_named_hallmark_info, save_hallmark_duration_correlation]):
        results_writer = pd.ExcelWriter('./results/result_tables.xlsx')

    if bootstrap:
        results_writer_bootstrap = pd.ExcelWriter('./results_april_25_2019/bootstrap_tam.xlsx')

    # Input files
    data_path = glob.glob('data/mutant/complete_analysis_tam.xlsx')
    legend_data_path = "data/mutant/legend.xlsx"

    # Capitalize title columns to match
    # dropcolumns = [i.title() for i in dropcolumns]

    # Optional input mapping for each column
    # Used when alternative names are included.
    mapping = {
        'MT array': {
            '5.1': '6',
            '6': '7',
            '7': '8',
            '8': '9',
            '9': '10',
            '10': '11',
            '11': '12',
            '12b': '13',
            '13b': '14',
            '14': '15',
            '15': '16',
            '11c': '12',
            '12c': '13',
            '17': '14',
        },
        'cell shape': {
            '5b': '5',
            '6b': '6',
            '3*': '3',
            '8': '6'
        },
        'nucleus position': {
            '5c': '5',
            '5*': '5',
            '2*': '2',
        }
    }
    invalid = {'n'}

    # ------------------------------------------------------------------------------------------ #
    # Input files
    # ------------------------------------------------------------------------------------------ #

    # Load all excel files.
    all_datafiles = [pd.io.excel.ExcelFile(i) for i in data_path]

    # Take the legend from the first file if we didn't define anything else.
    if legend_data_path is None:
        legend_data = all_datafiles[0]
    else:
        legend_data = pd.io.excel.ExcelFile(legend_data_path)

    # Check for duplicate names other than the legend.
    for file_0, file_1 in itertools.permutations(all_datafiles, 2):
        intersection = set(file_0.sheet_names).intersection(file_1.sheet_names)
        assert (not intersection or intersection == {'legend'})

    # ------------------------------------------------------------------------------------------ #
    # Read all data files
    # ------------------------------------------------------------------------------------------ #

    legend_df, short_legend_df = read_legend(legend_data, dropcolumns=dropcolumns)
    # Read stages/hallmarks and their transitions according to the initial scheme
    hallmark_df, hallmark_transition_df = read_hallmarks(legend_data, dropcolumns=dropcolumns)

    # Load all time line data into a dictionary of sample:DataFrames
    dropcolumns = [i.title() for i in dropcolumns]
    if 'Nucleus Positoin' in dropcolumns:
        dropcolumns[dropcolumns.index('Nucleus Positoin')] = 'Nucleus Position'
    all_data = read_timeseries_data(all_datafiles, dropcolumns=dropcolumns,
                                    max_step=max_step, mapping=mapping, invalid_entries=invalid)
    # ------------------------------------------------------------------------------------------ #
    # Process data into (more usable) formats
    # ------------------------------------------------------------------------------------------ #

    # Find all events in the complete data set
    temp = find_events(all_data, events_fill_na=events_fill_na)
    all_events, event_times, event_df, event_counts, event_df_grouped = temp

    event_data = []
    for key, value in all_events.items():
        df = value.copy().reset_index()
        df.insert(0, 'sample', key)
        event_data.append(df)

    all_events_df = split_sample_id(pd.concat(event_data, ignore_index=True))
    max_valid_range = 15
    valid_events = all_events_df[all_events_df.time_range <= max_valid_range]
    simultaneous_event_counts = (valid_events
                                 # First groupby to get the number of events
                                 # occurring at the same time-point
                                 .groupby(['flower', 'cell', 'time'])
                                 .size().reset_index(name='counts')
                                 # Second groupby to get the total for each of the counts
                                 .groupby('counts').size()).to_frame()
    simultaneous_event_counts.columns = ['counts']
    simultaneous_event_counts['percentage'] = (simultaneous_event_counts['counts'] /
                                               len(valid_events) * 100)

    # Count co-occurring states in the complete dataset
    temp = count_co_occurences(all_data, time_resample_interval=time_resample_interval,
                               resample_interpolate_function=resample_interpolate_function)
    all_df, combination_per_sample_df, combination_df = temp

    # Add in states that were not observed at all - no combinations with itself.
    for i, j in zip(*np.where(~legend_df.isna())):
        j = j+1
        try:
            combination_df.loc[pd.IndexSlice[i, j, i, j]]
        except TypeError:
            combination_df.loc[pd.IndexSlice[i, j, i, j]] = 0

    # Numbers for paper
    n_cells = len(np.unique(np.array(all_df[['cell', 'flower']].values, dtype=str), axis=0))
    n_samples = len(np.unique(all_df.flower))
    total_time = np.array([i.index[-1] for i in all_data.values()]).sum()
    n_hours = total_time.days * 24 + total_time.seconds // 3600
    n_all = sum(len(i) for i in all_data.values())
    n_all_nan = sum([np.all(pd.isna(i), axis=1).sum() for i in all_data.values()])
    n_some_nan = sum([np.any(pd.isna(i), axis=1).sum() for i in all_data.values()])

    # Co-occurrences
    unstacked = combination_df.unstack(level=[2, 3]).fillna(0).astype(int)
    unstacked.columns = unstacked.columns.droplevel(0)
    unstacked.sort_index(axis=0, inplace=True)
    unstacked.sort_index(axis=1, inplace=True)

    cumulative_group_indeces = np.nonzero(np.diff(unstacked.index.get_level_values(0)))[0]

    simple_index = np.array([short_legend_df.iloc[i, j-1] for i, j in unstacked.index.tolist()])
    # ND is repeated a few times, so replace with unique versions
    # Same with Ce, T and No.
    # Note that this will work but not display the correct one depending on which colums are dropped.
    simple_index = simple_index.astype('<U20')
    n = sum(simple_index == 'ND')
    if n > 1:
        simple_index[simple_index == 'ND'] = ["ND_no_1", "ND_no_2", "ND_rec_fluor", "ND_rec_vis"][:n]
    n = sum(simple_index == 'T')
    if n > 1:
        simple_index[simple_index == 'T'] = ["T_cs_tetr", "T_ma_tetr"][:n]
    n = sum(simple_index == 'No')
    if n > 1:
        simple_index[simple_index == 'No'] = ["No_nu_nn_1", "No_nu_nn_2"][:n]
    n = sum(simple_index == 'Ce')
    if n > 1:
        simple_index[simple_index == 'Ce'] = ["Ce_nc_1", "Ce_nc_2"][:n]

    # Do some work on the co-occurrence index to get it to print nicer.
    labels = np.arange(len(unstacked.index.codes[1]))
    unstacked.index.set_levels([short_legend_df.index.tolist(), simple_index], inplace=True)
    unstacked.columns.set_levels([short_legend_df.index.tolist(), simple_index], inplace=True)
    unstacked.index.set_codes(labels, level=1, inplace=True)
    unstacked.columns.set_codes(labels, level=1, inplace=True)

    percentage_df = ((unstacked / unstacked.values.diagonal()) * 100).fillna(0)

    # Find Hallmarks
    hallmark_methods = [
                        # ('score_count', lambda x: [x], lambda x, y: x),
                        # ('score_z_r1', lambda x: generate_neighbours(x, 1), z_score),
                        ('score_z_r2', lambda x: generate_neighbours(x, 2), z_score),
                        # ('score_z_r3', lambda x: generate_neighbours(x, 3), z_score),
                        ]

    # Optional bootstrap analysis and relevant settings.
    # Can be resampled randomly perobservation, per cell or per flower.
    if bootstrap:
        import time
        factor = 1.5
        seed = None
        n = 1000
        for method in hallmark_methods:
            t0 = time.time()
            r_random = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'random',
                                                             factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            t0 = time.time()
            r_cell = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'cell',
                                                           factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            t0 = time.time()
            r_flower = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'flower',
                                                             factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            sheets = ['random', 'cell', 'flower']
            data = [r_random, r_cell, r_flower]

            index_order = [i.title() for i in heatmap_plot_order if i.title() in data[0].columns.names]
            for df, sheet_name in zip(data, sheets):
                sheet_name = method[0] + '__' + sheet_name
                score = df.describe().T.sort_values(by='mean', ascending=False)
                score.index = score.index.reorder_levels(index_order)
                score.to_excel(results_writer_bootstrap, sheet_name=sheet_name)
                # Fill NaN values so they rank last but the total rank count stays the same
                rank = (df.fillna(-2).rank(1, ascending=False)
                        .describe().T.sort_values(by='mean', ascending=True))
                start = len(score.columns) + len(score.index.levels) + 1
                rank.index = rank.index.reorder_levels(index_order)
                rank.to_excel(results_writer_bootstrap, sheet_name=sheet_name, startcol=start)
        results_writer_bootstrap.save()

    # Remove time, flower and cell columns and all states with one or more NaNs
    all_df_cleaned = all_df.iloc[:, 3:]
    nans = np.any(pd.isna(all_df_cleaned), axis=1)
    all_df_cleaned = all_df_cleaned[~nans].astype(int)

    state_count_df = pd.DataFrame(all_df_cleaned.groupby(list(all_df_cleaned.columns)).size())
    state_count_df.columns = pd.Index(['count'])
    percentage = state_count_df['count'] / state_count_df['count'].sum() * 100
    state_count_df['percentage'] = percentage

    new_hallmarks = []
    new_hallmark_names = []
    for (name, neighbours_f, score_f) in hallmark_methods:
        # Calculate scores based on this measure
        score = score_hallmarks(state_count_df, neighbours_f, score_f)['score']
        # Save in the general DataFrame
        state_count_df = state_count_df.assign(**{name: score})
        # Create a state table based on the score
        if add_start_end_hallmark:
            add_stages = {tuple(i) for _, *i in hallmark_df.iloc[:, [0, -1]].T.itertuples()}
            score = score[[i not in add_stages for i in score.index.values]]
        new_stages = score.nlargest(n_hallmark_stages).index.to_frame()
        new_stages = new_stages.T
        new_stages.columns = range(min(len(new_stages.columns), n_hallmark_stages))
        # To have the same format as the hallmarks DataFrame
        mapping = {'Cell Shape': 'cell shape',
                   'Mt Array': 'MT array',
                   'Nucleolus Position': 'nucleolus position',
                   'Nucleus Position': 'nucleus positoin',
                   'Rec8/Chromatin': 'REC8/Chromatin',
                   'Tapetum': 'Tapetum',
                   }
        new_stages.index = [mapping[i] for i in new_stages.index]
        # Correct order to hallmark equivalent
        new_stages = new_stages.loc[hallmark_df.index.tolist()]
        # Add start and end states if required.
        if add_start_end_hallmark:
            a, b = add_stages
            new_stages = new_stages.assign(a=a, b=b)
        # Sort the stages
        new_stages = new_stages.sort_values(by=new_stages.index.tolist(), axis=1)
        # Custom sorting for the mutants - none of the sorting orders prevent states reverting.
        # This does not impact really the results, but mainly the landmark plot.
        new_stages = new_stages.iloc[:, [0, 1, 2, 3, 4, 5, 7, 6, 9, 10, 11, 8, 12, 13]]
        # Assign Hallmark names
        # r = range(n_hallmark_stages + (len(add_stages) if add_start_end_hallmark else 0))
        new_stages.columns = ["H{}".format(i) for i in range(len(new_stages.columns))]
        new_stages.index.name = 'hm_{}'.format(name)
        new_hallmarks.append(new_stages)
        new_hallmark_names.append(new_stages.index.name)

    hallmarks = [hallmark_df] + new_hallmarks
    hallmark_names = ['original'] + new_hallmark_names

    for name, df in zip(hallmark_names, hallmarks):
        column = get_state_hallmarks(state_count_df, df)
        state_count_df = state_count_df.assign(**{name: column})

    # Check correlations of duration of feature state combinations.

    # Only use events that mark the time spent in 1 single feature state
    times = event_df.query('(feature_1 == feature_2) '
                           'and ((before_2 - before_1) == 1) '
                           'and ((after_2 - after_1) == 1)')

    # Pre-process all groups, where each group is the timings of single feature state.
    dfs = []
    for state, df in times.groupby(event_df.columns[5:].tolist()):
        state = state[0], state[2]
        # Can we use the error somehow?
        # dfs.append((state, df.assign(error=df[['error_1', 'error_2']].sum(axis=1))
        #                    .set_index(["flower", "cell"])
        #                    .loc[:, ['time', 'error']]))
        dfs.append((state, df.set_index(["flower", "cell"])
                             .loc[:, ['time']]))

    if save_state_duration_correlation:
        result = check_duration_correlations(dfs, min_size=min_correlation_size,
                                             n_permutations=n_permutations)
        state_duration_correlation_df, combined_state_frames = result

    if save_hallmark_duration_correlation or save_named_hallmark_info:
        # Check the timeline and durations of stages/hallmarks in the complete dataset
        hallmark_transitions = []
        hallmark_correlations = []
        hallmark_combined_frames = []

        for name, df in zip(hallmark_names, hallmarks):
            timelines, named_hallmark_transition_df = get_hallmark_timelines(all_data, df)
            ordering = dict((j, i) for i, j in enumerate(df.columns.tolist()))

            hallmark_transitions.append(named_hallmark_transition_df)

            if save_hallmark_duration_correlation:
                df = named_hallmark_transition_df.copy()
                # Split sample column into flower and cell
                columns = df['sample'].str.rsplit('_', 1).str
                df.insert(0, 'cell', columns[1])
                df.insert(0, 'flower', columns[0])
                df.drop('sample', axis=1, inplace=True)
                # Drop invalid columns
                df = df[df.valid].drop('valid', axis=1)

                dfs = []
                for (from_state, to_state), df in df.groupby(['from', 'to']):
                    if ordering[from_state] == ordering[to_state] - 1:
                        dfs.append((from_state, df.set_index(["flower", "cell"])
                                                .rename({'time(min)': 'time'}, axis=1)
                                                .loc[:, ['time']]))
                if dfs:
                    result = check_duration_correlations(dfs, min_size=min_correlation_size,
                                                        n_permutations=n_permutations)
                else:
                    result = pd.DataFrame(), []

                hallmark_duration_correlation_df, combined_hallmark_frames = result

                hallmark_correlations.append(hallmark_duration_correlation_df)
                hallmark_combined_frames.append(combined_hallmark_frames)

    # ------------------------------------------------------------------------------------------ #
    # Section for excel output tables.
    # ------------------------------------------------------------------------------------------ #

    if save_event_info:
        df = event_df_grouped['time'].describe()
        df.to_excel(results_writer, sheet_name='pairwise_event_info',
                    merge_cells=True)
        df_sorted = df.sort_values(by='count', ascending=False)
        df_sorted.to_excel(results_writer, sheet_name='pairwise_event_info_by_count',
                           merge_cells=False)
        results_writer.save()

    if save_named_hallmark_info:
        for i, (name, df) in enumerate(zip(hallmark_names, hallmark_transitions)):
            df = df.groupby(['from', 'to']).describe()
            df.index.name = name
            spacing = 1 + len(df.columns) + len(df.index.levels)
            df.to_excel(results_writer, sheet_name='hallmark_info', startcol=i*spacing)

        results_writer.save()

    if save_state_duration_correlation:
        df = state_duration_correlation_df.sort_values(by='p-value_perm')
        df.to_excel(results_writer, sheet_name='state_duration_correlations')
        results_writer.save()

    if save_hallmark_duration_correlation:
        for i, (name, df) in enumerate(zip(hallmark_names, hallmark_correlations)):
            if len(df) == 0:
                continue
            df.index.name = name
            df = df.sort_values(by='p-value_perm')
            spacing = 1 + len(df.columns) + 1
            df.to_excel(results_writer, startcol=i*spacing,
                        sheet_name='hallmark_duration_correlations')
        results_writer.save()

    if save_co_occurence_sheets:
        unstacked.to_excel(results_writer, sheet_name='co-occurrence counts')
        percentage_df.to_excel(results_writer, sheet_name='co-occurrence percentages')
        results_writer.save()

    if save_hallmark_sheets:
        state_count_df.to_excel(results_writer, sheet_name='hallmark_by_hallmark',
                                merge_cells=False)
        state_count_df_sorted = state_count_df.sort_values(by='percentage', ascending=False)
        state_count_df_sorted.to_excel(results_writer, sheet_name='hallmark_by_percentage',
                                       merge_cells=False)

        # Calculate the coverage of each of the hallmark schemes and print to a single sheet.
        for i, name in enumerate(hallmark_names):
            df = state_count_df.groupby(name)['percentage'].sum()
            # In get_state_hallmarks we used the -> arrow to note stages classified as transitions.
            transition_stages = df.index.str.contains('->')
            transition_sum = df[transition_stages].sum()
            try:
                not_assigned = df.loc['']
            except KeyError:
                not_assigned = 0
            hallmarks_sum = df[~transition_stages].sum() - not_assigned
            index = ['total assigned', 'all hallmarks',
                     'all transitions', 'total unassigned'] + df.index.tolist()[1:]
            values = df.tolist()
            values = [hallmarks_sum + transition_sum,
                      hallmarks_sum, transition_sum] + values
            df = pd.DataFrame(data=values, index=index)
            df.columns = ['percentage']
            # Move H1x to the end to maintain the logical ordering of hallmark stages.
            to_move = df.index.str.match('H\d\d+')
            df = pd.concat([df[~to_move], df[to_move]])
            df.index.name = name
            spacing = 2 + len(df.columns)
            df.to_excel(results_writer, sheet_name='hallmark_tables', startcol=i*spacing)

        results_writer.save()

    # ------------------------------------------------------------------------------------------ #
    # Section for plotting
    # ------------------------------------------------------------------------------------------ #

    if plot_feature_chains:
        for c, feature in enumerate(legend_df.index):
            length = (~pd.isna(legend_df.loc[feature])).sum()
            r = range(1, (~pd.isna(legend_df.loc[feature])).sum() - 1)
            chain = [((c, i, i+1), (c, i+1, i+2)) for i in r]
            plot_event_hist_chain(event_times, chain, save=saveplots)

    # Plot data in nice timeline format
    if plot_series:
        for name, data in all_data.items():
            try:
                plot_time_series(data, name, saveplots)
            except ValueError:
                pass

    # Plot the distribution of the most common combinations
    if plot_hists:
        for key, _ in event_counts.most_common(50):
            times = event_times[key] + [(s, -t, e1, e2) for s, t, e1, e2 in event_times[key[::-1]]]
            plot_event_hist(event_times[key], key, legend_df, saveplots, errors=True, nbins=25)

    if plot_co_ocurrence:
        fig, textboxes = plot_heatmap(percentage_df, heatmap_plot_order)
        if saveplots:
            fig.savefig('results/figures/co-occurences/heatmap.png',
                        bbox_inches='tight', dpi=FIG_DPI,
                        bbox_extra_artists=textboxes)
            plt.close(fig)

    if plot_hallmark_scores:
        for name, _, _ in hallmark_methods:
            hallmark_scores = state_count_df[name].sort_values(na_position='first')
            ax = hallmark_scores.plot.bar()
            fig = ax.get_figure()
            try:
                labels = [str(tuple(int(i) for i in j))[1:-1] for j in hallmark_scores.index.values]
            except TypeError:
                labels = [str(j) for j in hallmark_scores.index.values]
            ax.set_xticklabels(labels, fontdict={'fontsize': 5})
            ax.set_xlabel('')
            ax.set_ylabel('score')
            ax.set_title('{}'.format(name))
            xmin, xmax = ax.get_xlim()
            ax.hlines([1, 2], xmin, xmax, linestyle='dashed', color='black', alpha=.5)

            sns.despine(fig)
            fig.tight_layout()
            if saveplots:
                fig.savefig('results/figures/hallmarks/hallmark_scores_{}.png'.format(name),
                            bbox_inches='tight', dpi=FIG_DPI)
                plt.close(fig)

    if plot_hallmark_graph:
        for t in hallmark_plot_max_interval_times:
            limited_fill_data = {k: ffillna_limited(v, t) for k, v in all_data.items()}
            all_transitions_df_ffill = get_all_transitions(limited_fill_data, fillna=None)
            transition_counts_ffill = (all_transitions_df_ffill[all_transitions_df_ffill['valid']]
                                       .groupby(['state_before', 'state_after'])['sample'].count())
            # diff = (transition_counts_ffill - transition_counts).replace(0, np.NaN).dropna()

            default_score = 'percentage'
            for name in hallmark_names:
                if 'hm_score' in name:
                    score_column = name[3:]
                else:
                    continue
                    score_column = default_score
                state_counts = state_count_df.loc[:, ['count', score_column, name]]
                plot_transition_hallmark_graph(state_counts, transition_counts_ffill,
                                               score_column=score_column, name=name,
                                               shape='circle', plot_other_nodes=True,
                                               set_colorbar=True, save=saveplots,
                                               rename_start_end=True, invalid_striketrough=False,
                                               fig_size=(12, 12), t_ffill=t, hallmark_initial='$\mathbf{\mathit{t}}$',
                                               dpi=FIG_DPI)

    # ------------------------------------------------------------------------------------------ #
