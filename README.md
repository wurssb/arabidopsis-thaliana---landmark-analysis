# Arabidopsis thaliana - Landmark Analysis
Landmark analysis as detailed in "Live cell imaging of meiosis in Arabidopsis thaliana - a landmark system". 
In `main.py` the analysis is done for the WT strain, while in `tam_mutant.py` the same analysis was performed for the mutant strain.
Settings can be changed to modify what is outputted under the header `Script settings` in both files.

The raw data can be found in `/data` for both the WT and the *tam* mutant.

## Requirements
Exact versions of libraries used are noted in the parentheses.

- Python 3.7+
- Numpy (1.15.4)
- Scipy (1.2.0)
- Pandas (0.24.1) including openpyxl (2.6.0) and xlrd (1.2.0)
- Matplotlib (3.0.2)
- Seaborn (0.9.0)
- NetworkX (2.2)

## Authors:
- Rik van Rosmalen
- In collaboration with:
    - Emma Keizer
    - Maria Prusicki
    - Christian Fleck
    - Arp Schnittger

## License:
This project is licensed under the MIT License - see the LICENSE file for details.

## Reference:

Live cell imaging of meiosis in Arabidopsis thaliana (2019); Prusicki, M. A., Keizer, E. M., van Rosmalen, R. P., Komaki, S., Seifert, F., Müller, K., Wijnker, E., Fleck, C., Schnittger, A.; eLife, 8, e42834; doi: [10.7554/eLife.42834](https://doi.org/10.7554/eLife.42834)

Live cell imaging of meiosis in Arabidopsis thaliana - a landmark system (2019); Maria Ada Prusicki, Emma Mathilde Keizer, Rik Peter van Rosmalen, Shinichiro Komaki, Felix Seifert, Katja Müller, Erik Wijnker, Christian Fleck, Arp Schnittger; bioRxiv 446922; doi: [10.1101/446922](https://doi.org/10.1101/446922) (preprint)

