#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Analysis script for the plant cycle data set.

Author: Rik van Rosmalen
Contact: rik.vanrosmalen@wur.nl / rikpetervanrosmalen@gmail.com
"""
import collections
import itertools
import re
import os
import glob
import datetime
import functools
import numbers

import numpy as np
import scipy
import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import networkx as nx


# Regex to extract flower, part of flower and cell
SPLIT_SAMPLE_REGEX = re.compile(r"^([0-9]+)_?(?:([LRU])_)?(.*)$")


def linewidth_from_data_units(linewidth, axis, reference='y'):
    """
    Convert a linewidth in data units to linewidth in points.

    Parameters
    ----------
    linewidth: float
        Linewidth in data units of the respective reference-axis
    axis: matplotlib axis
        The axis which is used to extract the relevant transformation
        data (data limits and size must not change afterwards)
    reference: string
        The axis that is taken as a reference for the data width.
        Possible values: 'x' and 'y'. Defaults to 'y'.

    Returns
    -------
    linewidth: float
        Linewidth in points

    Source: https://stackoverflow.com/a/35501485
            by Felix (https://stackoverflow.com/users/5950211/felix)
            Licensed under cc-by-sa: http://creativecommons.org/licenses/by-sa/3.0/
    """
    fig = axis.get_figure()
    if reference == 'x':
        length = fig.bbox_inches.width * axis.get_position().width
        value_range = np.diff(axis.get_xlim())
    elif reference == 'y':
        length = fig.bbox_inches.height * axis.get_position().height
        value_range = np.diff(axis.get_ylim())
    # Convert length to points
    length *= 72
    # Scale linewidth to value range
    return linewidth * (length / value_range)


def plot_time_series(data, name, save=False,
                     fig=None, ax=None):
    """Plot a time series of the data, making sure that lines do not overlap."""
    time = (data.index.values / (1e9 * 60)).astype(int)
    values = data.values

    width = 0.075
    offsets = {i+1: np.linspace(-i*width, i*width, i+1)
               for i in range(1, values.shape[1])}

    # Add offsets where lines have the same y value
    offset_values = np.array(values, dtype=float)
    for i, row in enumerate(values):
        for value, count in collections.Counter(row).items():
            if count > 1:
                idx = row == value
                offset_values[i, idx] += offsets[count]

    if fig is None and ax is None:
        fig, ax = plt.subplots()

    if fig is not None:
        fig.set_size_inches(15, 10, forward=True)
    ax.yaxis.set_ticks(np.arange(0, 16))
    ax.set_ylim(0, 17)
    ax.set_ylabel('State')
    ax.set_xlim(time[0], time[-1])
    ax.set_xlabel('Time (min)')
    ax.set_title(name)

    palette = itertools.cycle(sns.color_palette())

    linewidth = linewidth_from_data_units(width*2, ax, 'y')
    lines = []
    for y in offset_values.T:
        # Calculate interpolated points
        nans = np.isnan(y)
        interpolated = np.interp(time[nans], time[~nans], y[~nans],
                                 left=np.nan, right=np.nan)
        # Keep only values next to NaN
        nan_y = np.zeros(y.shape) + np.nan
        nan_indeces = np.nonzero(nans)[0]
        keep = np.setdiff1d(np.union1d(nan_indeces - 1,
                                       nan_indeces + 1),
                            nan_indeces)
        keep = keep[(keep > 0) & (keep < len(y))]
        nan_y[keep] = y[keep]
        # Copy over interpolated values
        nan_y[nans] = interpolated

        color = next(palette)
        # Line for actual values
        lines.extend(ax.plot(time, y, color=color,
                             linewidth=linewidth, solid_capstyle='butt'))
        # Alternative line for interpolated NaN values
        ax.plot(time, nan_y, linestyle=':', color=color, alpha=.75,
                linewidth=linewidth, solid_capstyle='butt')
        # Finally, a marker at the points
        # where we switch from real to interpolated data.
        ax.scatter(time[keep], y[keep], s=12*linewidth, color=color, marker='|')

    ax.legend(lines, list(data.columns))

    if fig is not None:
        sns.despine(fig)
        fig.tight_layout()
        if save:
            fig.savefig('results/figures/timeseries/{}.png'.format(name),
                        bbox_inches='tight', dpi=300)
            plt.close(fig)
    return ax


def bin_with_errors(values, errors, n_bins=101):
    """Bin values with errors assuming a uniform distribution."""
    # Get the extremes for the bin borders.
    ma = (values + errors).max()
    mi = (values - errors).min()
    bins = np.linspace(mi, ma, n_bins)
    counts = np.zeros(bins.shape)
    # For each value, split their count over the number of bins as defined
    # by their errors.
    first_bin = np.digitize(values - errors, bins)
    last_bin = np.digitize(values + errors, bins)

    for fb, lb in zip(first_bin, last_bin):
        counts[fb:lb] += 1.0 / (lb - fb)

    bins = np.append(bins, ma + (ma - mi) / n_bins)
    counts = np.append(counts, 0)
    return bins, counts


def plot_event_hist(values, key, legend, save=False, errors=False, nbins=100,
                    fig=None, ax=None):
    """Plot a histogram of time between events."""
    times = np.array([i[1] for i in values])
    if fig is None and ax is None:
        fig, ax = plt.subplots()

    # Make a simple histogram
    if not errors:
        ax.hist(times, bins=nbins)
        ax.set_ylabel('Counts')
    else:
        errors = np.array([i[2] for i in values])
        # This is actually the number of edges, so we take 1 extra like matplotlib does.
        nbins = nbins + 1
        bins, counts = bin_with_errors(times, errors, nbins)

        ax.plot(bins, counts, drawstyle='steps')
        ax.set_ylabel('Distributed counts (total = {})'.format(len(times)))
        ax.set_ylim(bottom=0)

    format_args = {'f_1': legend.index[key[0][0]],
                   'f_2': legend.index[key[1][0]],
                   's_1_b': legend.iloc[key[0][0], key[0][1]],
                   's_1_a': legend.iloc[key[0][0], key[0][2]],
                   's_2_b': legend.iloc[key[1][0], key[1][1]],
                   's_2_a': legend.iloc[key[1][0], key[1][2]],
                   }
    ax.set_title('{f_1}: {s_1_b} -> {s_1_a} \n'
                 '{f_2}: {s_2_b} -> {s_2_a}'.format(**format_args))
    ax.set_xlabel('Minutes')

    if fig is not None:
        sns.despine(fig)
        fig.tight_layout()
        if save:
            name = '-'.join(str(i).replace(', ', '_') for i in key)
            fig.savefig('results/figures/event_hist/{}.png'.format(name),
                        bbox_inches='tight', dpi=FIG_DPI)
            plt.close(fig)
    return ax


def plot_event_hist_chain(event_times, chain, start_function=np.median, spacer=1.5,
                          save=False, fig=None, ax=None):
    """Plot a chain of event timing histograms.

    The start function is used to determine the absolute value from where
    on to start the next distribution of events.
    """
    if fig is None and ax is None:
        fig, ax = plt.subplots()

    start = 0
    old_start = 0
    for n, event in enumerate(chain):
        points = event_times[event] + event_times[event[::-1]]
        if not points:
            print("No points for event {0[0]} -> {0[1]} in chain.".format(sorted(event)))
            continue
        values = np.array([start + i[1] for i in points])
        errors = np.array([i[2] + i[3] for i in points])
        start, old_start = start_function(values), start
        bins, counts = bin_with_errors(values, errors, n_bins=101)
        if n == 0:
            bin_start = bins[0]
        # Determine scaling so max = 1
        line = ax.plot(bins, (counts / counts.max()) - n * spacer,
                       drawstyle='steps')[0]
        # Add bottom line at y = -n
        ax.hlines(-n * spacer, min(old_start, bins[0]), bins[-1],
                  color=line.get_color(), linewidth=line.get_linewidth(),
                  linestyle='dashed')

        # Add indication for amount of samples in distribution:
        text_start = min(old_start, values.min()) - 10
        if n == 0:
            text_start = values.min() - 10
        ax.text(text_start, -(n) * spacer, 'n={}'.format(len(points)),
                size='small', horizontalalignment='right', verticalalignment='bottom')
        if n != len(chain) - 1:
            # Add marker + line to next series starting point.
            ax.scatter([start, start], [-n * spacer, -(n+1) * spacer],
                       color='red', marker='o', s=10)
            ax.vlines(start, -n * spacer, -(n+1) * spacer,
                      color='red', linestyle='dotted')
        else:
            # Only single marker for the final entry.
            ax.scatter([start], [-n * spacer],
                       color='red', marker='o', s=10)

    ax.set_xlim(bin_start-10, bins[-1])
    ax.set_yticks([], [])
    ax.set_xlabel('time (min) starting from median of last state (red).')
    ax.yaxis.set_visible(False)

    if fig is not None:
        sns.despine(fig, left=True)
        fig.tight_layout()
        if save:
            flat = list(itertools.chain(*[sorted(i) for i in chain]))
            name = '_'.join(''.join(str(j) for j in i)
                            for i in (flat[::2] + flat[-1:]))
            fig.savefig('results/figures/chains/chain_{}.png'.format(name),
                        bbox_inches='tight', dpi=FIG_DPI)
            plt.close(fig)
    return ax


def plot_transition_hallmark_graph(state_counts, transition_counts, score_column,
                                   name='', shape='circle', plot_other_nodes=True,
                                   center_color_at_zero=True, set_colorbar=False,
                                   save=False, rename_start_end=True, invalid_striketrough=False,
                                   fig_size=None, t_ffill='', hallmark_initial='H', dpi=300):
    df = state_counts
    edge_counts = transition_counts
    # Get transitions
    transitions = df[name].str.contains('->')
    # Get hallmarks
    hallmarks = np.logical_and(~transitions, df[name].str.match('^H'))
    # Get others
    others = np.logical_and(~transitions, ~hallmarks)
    # Build graph structure
    G = nx.DiGraph()
    # Add nodes, set size / color etc.
    # Separate hallmarks by the number of stages that need to change
    x = 0
    y_at_x = collections.defaultdict(int)
    # Centre y around 0 going up and down alternatively for more nodes that are on
    # the same x coordinate (Series formula from: oeis.org/A001057)
    y_function = lambda x: (1 - (np.power(-1, x) * (2 * x + 1))) / 4
    steps = 0
    hallmark_locations = {}
    positions = {}
    sort_index = df[hallmarks].hm_score_z_r2.str.strip('H').astype(int).sort_values().index
    sorted_hallmarks = df[hallmarks].loc[sort_index]
    for i, (state, count, score, stage_name) in enumerate(sorted_hallmarks.itertuples()):
        x = x + steps
        y = y_at_x[x]
        y_at_x[x] += 1

        positions[state] = (x, y_function(y))
        G.add_node(state, stage_name=stage_name,
                   count=count, score=score,
                   hallmark=True, transition=False)

        hallmark_locations[stage_name] = x
        try:
            steps = (np.array(sorted_hallmarks.index[i + 1]) - np.array(state))
            steps = steps[steps > 0].sum()
        except IndexError:
            steps = 0

    sorted_transitions = df[transitions].sort_values('count', ascending=False)
    for state, count, score, stage_name in sorted_transitions.itertuples():
        from_hallmark, to_hallmark = stage_name.split(' -> ')
        try:
            difference = (np.array(state) -
                          (np.array(df[df[name] == from_hallmark].index[0]))).sum()
            x = hallmark_locations[from_hallmark] + difference
        except IndexError:
            # The hallmark state could not be found, but we can try it from the other side.
            difference = (-np.array(state) +
                          (np.array(df[df[name] == to_hallmark].index[0]))).sum()
            x = hallmark_locations[to_hallmark] - difference

        y = y_at_x[x]
        y_at_x[x] += 1

        positions[state] = (x, y_function(y))
        G.add_node(state, stage_name=stage_name,
                   count=count, score=score,
                   hallmark=False, transition=True)

    if plot_other_nodes:
        valid_array = np.array(df[~others].index.tolist())
        sorted_others = df[others].sort_values('count', ascending=False)
        for state, count, score, _ in sorted_others.itertuples():
            # Get closest valid node by numerical difference
            differences = (np.array(state) - valid_array).sum(axis=1)
            differences_abs = np.abs(np.array(state) - valid_array).sum(axis=1)
            valid_index = np.argmin(differences_abs)
            x_closest, _ = positions[df[~others].iloc[valid_index].name]
            difference = differences[valid_index]
            x = x_closest + difference

            y = y_at_x[x]
            y_at_x[x] += 1

            positions[state] = (x, y_function(y))
            G.add_node(state, stage_name='',
                       count=count, score=score,
                       hallmark=False, transition=False)

    # Add edges between the nodes we've added.
    # Note that some nodes might not have edges between each other.
    # Also note that not all edges in edge_counts might have their nodes represented
    # depending on how we preprocessed the nodes / edges DataFrames with respect to
    # missing values.
    # for node1, node2 in itertools.combinations(G.nodes, 2):
    #     try:
    #         count = edge_counts[node1, node2]
    #     except KeyError:
    #         continue
    #     else:
    #         G.add_edge(node1, node2, count=count)

    # Add edges between the nodes we've added.
    # Also note that not all edges in edge_counts might have their nodes represented
    # depending on how we preprocessed the nodes / edges DataFrames with respect to
    # missing values.
    for (node1, node2), count in edge_counts.iteritems():
        if len(node1) == 1:
            node1 = node1[0]
        if len(node2) == 1:
            node2 = node2[0]
        if node1 in G and node2 in G:
            G.add_edge(node1, node2, count=count)
        # else:
            # print("Missig node in graph: {} or {}".format(node1, node2))

    # Add extra spacing around the positions
    x_space = 100
    y_space = 10
    positions = {state: (x*x_space, y*y_space) for state, (x, y) in positions.items()}
    min_x = min([x for x, y in positions.values()])
    max_x = max([x for x, y in positions.values()])

    r = 50
    if shape == 'circle':
        # Increase x a little bit so the first node and the last node do not overlap.
        max_x = max_x / len(hallmark_locations) * (len(hallmark_locations) + 1)
        positions = {state: (((r + y) * np.sin(2*np.pi * x / max_x)),
                             ((r + y) * np.cos(2*np.pi * x / max_x)))
                     for state, (x, y) in positions.items()}

    # Determine node sizes
    base_size = 25
    size_factor = 75
    sizes = {state: base_size + size_factor * np.log(count)
             for state, count in G.nodes(data='count')}

    # Determine node colors
    # Pick a normalizer from: LogNorm, Normalize, PowerNorm or SymLogNorm
    if df[score_column].min() >= 0:
        center_color_at_zero = False
    if center_color_at_zero:
        normalizer_p = matplotlib.colors.Normalize(vmin=0,
                                                   vmax=df[score_column].max())
        cmap_p = matplotlib.cm.YlOrRd
        mapper_p = matplotlib.cm.ScalarMappable(norm=normalizer_p, cmap=cmap_p)

        normalizer_n = matplotlib.colors.Normalize(vmin=df[score_column].min(),
                                                   vmax=0)
        cmap_n = matplotlib.cm.Blues_r
        mapper_n = matplotlib.cm.ScalarMappable(norm=normalizer_n, cmap=cmap_n)

        colors = {state: mapper_p.to_rgba(score) if score >= 0 else mapper_n.to_rgba(score)
                  for state, score in G.nodes(data='score')}
    else:
        normalizer = matplotlib.colors.Normalize(vmin=df[score_column].min(),
                                                 vmax=df[score_column].max())
        cmap = matplotlib.cm.RdYlBu_r
        mapper = matplotlib.cm.ScalarMappable(norm=normalizer, cmap=cmap)

        colors = {state: mapper.to_rgba(score) for state, score in G.nodes(data='score')}

    # Determine edge color
    # Add extra offset to avoid to white colors
    if G.edges(data='count'):
        edge_count_max = max(count for _, _, count in G.edges(data='count'))
    else:
        edge_count_max = 1

    # Set a very low vmin, so we start in grey
    normalizer_edges = matplotlib.colors.Normalize(vmin=-edge_count_max / 2,
                                                   vmax=edge_count_max)
    cmap_edges = matplotlib.cm.Greys
    mapper_edges = matplotlib.cm.ScalarMappable(norm=normalizer_edges, cmap=cmap_edges)
    edge_colors = {(s1, s2): mapper_edges.to_rgba(count)
                   for s1, s2, count in G.edges(data='count')}

    base_edge_width = 1
    edge_with_factor = 2.5
    edge_width = {(s1, s2): base_edge_width + edge_with_factor * (count / edge_count_max)
                  for s1, s2, count in G.edges(data='count')}

    # Plot graph
    fig, ax = plt.subplots()
    # For nodes: Size => count, color => score
    nodes = df[others].index
    if plot_other_nodes and nodes.size:
        face_alpha = .5
        edge_alpha = 1
        p = nx.draw_networkx_nodes(nodes, ax=ax, pos=positions,
                                   node_color=[colors[i] for i in nodes],
                                   node_size=[sizes[i] for i in nodes])
        p.set_alpha(face_alpha)
        p.set_edgecolors('gray')
        if invalid_striketrough:
            p.set_hatch('///')
        # Unfortunately, you currently cannot change face and edge colors of a patch independently.
        # face_colors = p.get_facecolors()
        # face_colors[:, -1] = face_alpha
        # p.set_facecolors(face_colors)
        # edge_colors = p.get_edgecolors()
        # edge_colors[:, -1] = edge_alpha
        # p.set_edgecolors(edge_colors)

    nodes = df[transitions].index
    if nodes.size:
        nx.draw_networkx_nodes(nodes, ax=ax, pos=positions,
                            node_color=[colors[i] for i in nodes],
                            node_size=[sizes[i] for i in nodes],
                            edgecolors='gray')
    # Transitions are drawn first, then hallmarks so the hallmarks are on top
    nodes = df[hallmarks].index
    if nodes.size:
        nx.draw_networkx_nodes(nodes, ax=ax, pos=positions,
                            node_color=[colors[i] for i in nodes],
                            node_size=[sizes[i] for i in nodes],
                            edgecolors='k')

    # For hallmark nodes, draw labels
    labels = dict(zip(df[hallmarks].index, df[hallmarks][name]))
    if rename_start_end:
        updated_labels = {}
        # max_label = df[hallmarks][name].str.extract('(.*([0-9]+).*)').max()[0]
        max_index = df[hallmarks][name].str.extract('.*?([0-9]+).*').astype(int).to_numpy().argmax()
        max_label = df[hallmarks][name].iloc[max_index]
        for k, v in labels.items():
            if v == 'H0':
                v = 'Start'
            elif v == max_label:
                v = 'End'
            updated_labels[k] = v.replace('H', hallmark_initial)
        labels = updated_labels
    nx.draw_networkx_labels(G, pos=positions, labels=labels,
                            font_size=8, font_weight='bold')

    # For edges: opacity/width => count
    # First draw edges between valid states with a solid line.
    G_valid = G.subgraph(df[~others].index)
    if G_valid.edges:
        nx.draw_networkx_edges(G_valid, ax=ax, pos=positions, arrow=True,
                               node_size=[sizes[i] for i in G_valid.nodes],
                               edge_color=[edge_colors[i] for i in G_valid.edges],
                               width=[edge_width[i] for i in G_valid.edges])

    # Draw edges that are to or from (or both) a invalid node with a dashed line.
    bad_edges = list(G.edges - G_valid.edges)
    if bad_edges:
        arrows = nx.draw_networkx_edges(G, ax=ax, edgelist=bad_edges,
                                        pos=positions, arrrow=True,
                                        node_size=[sizes[i] for i in G.nodes],
                                        edge_color=[edge_colors[i] for i in bad_edges],
                                        width=[edge_width[i] for i in bad_edges])
        # NetworkX appears to ignore the line_style argument so do it manually.
        for arrow in arrows:
            arrow.set_linestyle('dashed')

    # Final figure clean-up.
    ax.set_axis_off()
    if shape == 'circle' and fig_size is None:
        fig.set_size_inches(15, 15)
    elif fig_size is None:
        fig.set_size_inches(15, 5)
    else:
        fig.set_size_inches(*fig_size)
    if set_colorbar:
        x = .15
        y = .15
        width = .025
        height = .1
        if center_color_at_zero:
            cb_ax = fig.add_axes([x, y, width / 2, height])
            cb = matplotlib.colorbar.ColorbarBase(cb_ax, cmap=cmap_n, norm=normalizer_n)
            cb_ax = fig.add_axes([x, y + height, width / 2, height])
            cb = matplotlib.colorbar.ColorbarBase(cb_ax, cmap=cmap_p, norm=normalizer_p)
        else:
            cb_ax = fig.add_axes([x, y, width, height])
            cb = matplotlib.colorbar.ColorbarBase(cb_ax, cmap=cmap, norm=normalizer)

    if save:
        fig.savefig('results/figures/hallmarks/graph_{}_{}.png'.format(name, t_ffill),
                    bbox_inches='tight', dpi=dpi)
        plt.close(fig)


def plot_heatmap(percentage_df, heatmap_plot_order):
    """Plot the heatmap with all the states."""
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 10)
    # Swap order
    available = set(percentage_df.index.get_level_values(0))
    heatmap_plot_order = [i for i in heatmap_plot_order if i in available]
    percentage_df = percentage_df[heatmap_plot_order].T[heatmap_plot_order].T

    # Drop empties
    to_drop = percentage_df.index[percentage_df.sum() == 0]
    percentage_df = percentage_df.drop(to_drop, axis=0).drop(to_drop, axis=1)

    # Create new labels
    labels = []
    categories = []
    current = 1
    last_category = ''
    for i, j in percentage_df.index:
        if i == last_category:
            current += 1
            labels.append(str(current))
        else:
            last_category = i
            current = 1
            labels.append(str(current))
            categories.append(i.capitalize())

    # Plot heatmap
    xticklabels = yticklabels = labels
    group = sns.heatmap(percentage_df, linewidths=1.0, cmap="Blues",
                        ax=ax, xticklabels=xticklabels, yticklabels=yticklabels, square=True,
                        cbar_kws={'shrink': .2})

    # Draw some extra lines to separate the groups
    xmin, xmax = sorted(ax.get_xlim())
    ymin, ymax = sorted(ax.get_ylim())
    ticks = ax.get_xticks()
    group_indeces = np.where(np.diff([int(i) for i in labels]) != 1)[0]
    y_ticks = ticks[group_indeces] + .5
    x_ticks = ticks[group_indeces] + .5

    # Draw labels for each group on both the x and y axis
    category_labels = [i.replace(' ', '\n').replace('/', ' /\n') if len(i) > 10 else i
                for i in heatmap_plot_order]
    indices = [0] + list(x_ticks) + list(ticks[-1:] + .5)
    x_lim = xmin - 2.5
    y_lim = ymax + 2  # Y-axis is reversed so we have to take max + 2

    textboxes = []
    for start, end, label in zip(indices, indices[1:], category_labels):
        mid = (start + end) / 2
        textboxes.append(ax.text(x_lim, mid, label.capitalize(), rotation=90,
                                 horizontalalignment='center',
                                 verticalalignment='center',
                                 fontweight='medium'))
        textboxes.append(ax.text(mid, y_lim, label.capitalize(),
                                 horizontalalignment='center',
                                 verticalalignment='top',
                                 fontweight='medium'))

    # These are added by Seaborn automatically so hide them.
    ax.xaxis.label.set_visible(False)
    ax.yaxis.label.set_visible(False)

    ax.hlines(y_ticks, xmin-.5, xmax, colors='gray', alpha=1,
                linewidth=1, linestyle='dashed', clip_on=False)
    ax.vlines(x_ticks, ymin, ymax+.5, colors='gray', alpha=1,
                linewidth=1, linestyle='dashed', clip_on=False)

    fig.tight_layout(pad=3)
    return fig, textboxes


def split_sample_id(df, drop_sample=True):
    """Extract the flower and cell number from the sample number and update the df in-place."""
    columns = df['sample'].str.strip().str.extract(SPLIT_SAMPLE_REGEX).fillna('')
    if drop_sample:
        df.drop('sample', axis=1, inplace=True)
    df.insert(0, 'cell', columns[2])
    df.insert(0, 'flower', columns[0] + columns[1])
    return df


def count_co_occurences(all_data, time_resample_interval='1T', resample_interpolate_function=None):
    """Count co-occurrences (Time two states of two features spend together).

    Time will be resampled and interpolated for all datasets. By default, nearest neighbour
    re-sampling on a one minute resolution is used. Duplicate time-points are dropped, with
    the first of the duplicate being kept.
    """
    if resample_interpolate_function is None:
        resample_interpolate_function = lambda x: x.nearest()

    n_combinations = 2  # Only pairwise for now
    combination_list = []
    total_list = []

    all_data_resampled = []
    for name, data in all_data.items():
        # To resample we need to drop duplicates
        drop = data.index.duplicated(keep='first')
        data = data[~drop]
        try:
            data = resample_interpolate_function(data.resample(time_resample_interval))
        except ValueError:
            period = np.unique(np.diff(data.index.astype(int)))
            # Resample might fail if we already have the correct time sample for some reasons,
            # so check this and continue if possible or re-raise the error if not.
            if (len(period) == 1 and
                    period[0] == pd.tseries.frequencies.to_offset(time_resample_interval).nanos):
                pass
            else:
                print(name)
                raise

        data2 = data.copy()
        data2['sample'] = name
        split_sample_id(data2)
        # cell, flower = name.rsplit('_', 1)
        # data2.insert(0, 'cell', cell)
        # data2.insert(0, 'flower', flower)
        all_data_resampled.append(data2)
        # We could do combinations instead of permutations but this is easier to work with
        # in the resulting dataframe.
        permutations = itertools.permutations(range(data.columns.size), r=n_combinations)
        # We can use the duplicates to get the totals.
        duplicates = ((column, column) for column in range(data.columns.size))

        for columns in itertools.chain(permutations, duplicates):
            # Same as data.groupby(colums).size() but faster
            pairs, counts = np.unique(data.values[:, columns], return_counts=True, axis=0)
            nans = np.any(np.isnan(pairs), axis=1)

            for pair, count in zip(pairs[~nans], counts[~nans]):
                combination_list.append((name,
                                         columns[0], int(pair[0]),
                                         columns[1], int(pair[1]),
                                         count))

    column_names = 'sample feature_1 state_1 feature_2 state_2 count'.split()
    combination_per_sample_df = pd.DataFrame(data=combination_list,
                                             columns=column_names)
    # Split sample column into flower and cell
    # columns = combination_per_sample_df['sample'].str.rsplit('_', 1).str
    # combination_per_sample_df.insert(0, 'cell', columns[1])
    # combination_per_sample_df.insert(0, 'flower', columns[0])
    # combination_per_sample_df.drop('sample', axis=1, inplace=True)
    split_sample_id(combination_per_sample_df)
    # Same data but summed over all samples.
    combination_df = combination_per_sample_df.groupby(column_names[1:-1]).sum()

    # All data in one frame
    all_df = pd.concat(all_data_resampled, sort=False).reset_index()

    return all_df, combination_per_sample_df, combination_df


def read_legend(legend_file, dropcolumns=False):
    """Read the legend and create DataFrames with the full and abbreviated names."""
    # Parse legend
    legend_df = pd.read_excel(legend_file, 'legend',
                              skiprows=12, skipfooter=15,
                              usecols='A:Q',
                              header=None, index_col=0)
    # Remove second rows
    legend_df = legend_df.iloc[::2]
    # Update index name
    legend_df.index.name = 'feature'

    # Note here the columns are actually the rows!
    if dropcolumns:
        legend_df.drop(dropcolumns, axis=0, inplace=True)

    # legend with short names in parenthesis only.
    short_legend_df = legend_df.replace(r".*\((.*)\).*", r"\1", regex=True)
    return legend_df, short_legend_df


def read_hallmarks(legend_file, dropcolumns=False):
    """Extract the hallmarks from the legend table."""
    # Parse hallmark mapping
    hallmark_df = pd.read_excel(legend_file, 'legend',
                                skiprows=28, skipfooter=4,
                                header=0, index_col=0)
    if dropcolumns:
        hallmark_df.drop(dropcolumns, axis=0, inplace=True)

    hallmark_transitions = np.diff(hallmark_df, axis=1)
    columns = ['_'.join((hallmark_df.columns[i], hallmark_df.columns[i+1]))
               for i in range(hallmark_df.columns.size - 1)]
    hallmark_transition_df = pd.DataFrame(data=hallmark_transitions,
                                          columns=columns,
                                          index=hallmark_df.index.copy())
    return hallmark_df, hallmark_transition_df


def read_timeseries_data(datafiles, dropcolumns=False, max_step=15, mapping=None,
                         invalid_entries=None):
    """Read all timeseries from the datafiles.

    Skips excel sheets called 'legend'.
    Entries more then max_step (15 minutes) apart get 1 NaN inserted in-between.
    """
    all_data = {}

    sheets = []
    for datafile in datafiles:
        sheets.extend((datafile, sheet) for sheet in datafile.sheet_names)

    # Parse data sheets
    for datafile, sheet_name in sheets:
        if sheet_name == 'legend':
            continue

        # Actual data is in B4 (or C4, if there is an interval column)
        data = pd.read_excel(datafile, sheet_name,
                             skiprows=3, skipfooter=0,
                             usecols='B:H',
                             header=0, index_col=0,
                             dtype=str)
        if 'interval' in data.index.name:
            data = pd.read_excel(datafile, sheet_name,
                                 skiprows=3, skipfooter=0,
                                 usecols='C:I',
                                 header=0, index_col=0,
                                 dtype=str)

        # Remove whitespace from strings
        data = data.apply(lambda x: x.str.strip())

        # Replace n with NaN again incase they had whitespace.
        if invalid_entries is not None:
            for entry in invalid_entries:
                data = data.replace(entry, np.NaN)

        if mapping is not None:
            # df.replace cannot handle overlapping keys and values.
            # so we use map instead.
            for key, value in mapping.items():
                data[key] = data[key].map(value).fillna(data[key])

        # Mash all values into numbers.
        data = data.apply(pd.to_numeric, errors='raise')

        # Some samples have excel dates instead, and some of those are based on
        # 1990, so we need to detect and fix this.
        if data.index.dtype == np.object:
            new_index = []
            for t in data.index:
                if isinstance(t, datetime.time):
                    # Ignore seconds
                    t = t.hour * 60 + t.minute
                elif isinstance(t, datetime.datetime):
                    # Ignore years/months
                    t = (t.day * 24 + t.hour) * 60 + t.minute
                else:
                    raise ValueError("Cannot convert index of {}:{} "
                                     "to minutes! Type of {} detected."
                                     .format(datafile, sheet_name, type(t)))
                new_index.append(t)
            # Finally replace index (keeping the old name!)
            data.index = pd.Index(data=new_index, name=data.index.name)

        # Sometimes there is a double 0:00 point. Remove the first one.
        n_zeros = (data.index == 0).sum()
        if n_zeros > 1:
            data = data[n_zeros-1:]

        # Remove invalid points where neither the data nor the index exist.
        invalid = pd.isna(data.index) & np.all(pd.isna(data), axis=1)
        data = data.loc[~invalid]

        # Remove invalid data at the end.
        data = data.loc[:data.last_valid_index()]

        # Create proper time index
        data.index = pd.TimedeltaIndex(data.index, unit='m')

        # Remove white-space from the column labels
        data.columns = [i.strip().title() for i in data.columns]

        # Check if there are no big time step differences
        # Otherwise we insert a NaN row to signify the lack of data.
        stepsizes = np.diff(data.index.values.astype(int)) / (1e9 * 60)
        if np.any(stepsizes > max_step):
            insertion_points = np.where(stepsizes > max_step)[0] + 1

            # Prepare new data
            t_before = data.index[insertion_points - 1].values
            t_after = data.index[insertion_points].values
            t_points = t_before + (t_after - t_before) / 2
            point_data = [[np.nan] * len(data.columns)] * len(t_points)

            # Create DataFrame with entries at the right time points
            new_df = pd.DataFrame(index=t_points, data=point_data)
            new_df.columns = data.columns

            # Combine and let sort by the time index to put values in the
            # right position again.
            data = data.append(new_df).sort_index()

        if dropcolumns:
            data.drop(dropcolumns, axis=1, inplace=True)

        all_data[sheet_name] = data
    return all_data


def find_events(all_data, events_fill_na=True):
    """Find events in the data.

    An event is defined as a change in one of the features.
    This will return:
        (1) - a dictionary of DataFrames of events per sample
        (2) - a dictionary of list of times between events (pairwise)
        (3) - a DataFrame with the same data as (2)
        (4) - a counter of how many times an event occurred
        (5) - a grouped version of (3) indexed by pairs of events

    TODO: Refactor this into a one or two outputs and use those for everything
    """
    # Convert into event timeline
    all_events = {}
    for name, data in all_data.items():
        # Because we ask for bigger then 1, this filters out any NaNs as a bonus.
        time_index, feature_index = np.nonzero(np.diff(data, axis=0) > 0)
        # Alternatively, we can forward fill the NaNs and just give a bigger range.
        if events_fill_na:
            time_index, feature_index = np.nonzero(np.diff(data.fillna(method='ffill'),
                                                           axis=0) > 0)
        if not time_index.size:
            # print("No events happened in {}".format(name))
            continue

        # Retrieve the states before and after
        linear_index = np.arange(len(time_index))

        # For the before state and time we might need to backtrack if NaN values are involved.
        if events_fill_na:
            before_time = []
            for t, f in zip(time_index, feature_index):
                before_time.append(data.iloc[:t + 1, f].last_valid_index())
            before_time = pd.TimedeltaIndex(before_time)
            # Note that here we have a TimedeltaIndex so we use .loc
            # Below we have a numbered index so we use .iloc
            before_state = data.loc[before_time].values[linear_index, feature_index]
        else:
            before_time = data.index[time_index]
            before_state = data.iloc[time_index].values[linear_index, feature_index]

        # The after states are straightforward, since we used a forward fill earlier.
        after_time = data.index[time_index + 1]
        after_state = data.iloc[time_index + 1].values[linear_index, feature_index]

        # As the transition time, take the middle point in-between the two values
        # and the uncertainty range around it.
        time = (after_time - before_time) / 2 + before_time
        time_range = ((after_time - before_time) / 2).total_seconds() / 60

        event_data = list(zip(time_range, feature_index,
                              before_state.astype(int), after_state.astype(int)))

        events = pd.DataFrame(index=time, data=event_data)
        events.index.name = 'time'
        events.columns = pd.Index(['time_range', 'feature', 'before', 'after'])
        events.sort_index(inplace=True)
        all_events[name] = events

    # Count time between events within datasets
    # This is easier to use to query specific events
    event_times = collections.defaultdict(list)
    event_list = []
    for name, events in all_events.items():
        for i, (t1, event_1) in enumerate(events.iterrows()):
            for t2, event_2 in events.iloc[i + 1:].iterrows():
                # error range, feature, before, after
                r1 = event_1[0]
                f1, b1, a1 = (int(i) for i in event_1[1:])
                r2 = event_2[0]
                f2, b2, a2 = (int(i) for i in event_2[1:])
                key = ((f1, b1, a1),
                       (f2, b2, a2))
                dt = (t2 - t1).total_seconds() / 60
                event_times[key].append((name, dt, r1, r2))
                event_list.append((name, dt, r1, r2, f1, b1, a1, f2, b2, a2))

    # Can also be used as a DataFrame.
    columns = ('sample time error_1 error_2 '
               'feature_1 before_1 after_1 feature_2 before_2 after_2'.split())
    event_df = pd.DataFrame(data=event_list, columns=columns)
    # Split sample column into flower and cell
    split_sample_id(event_df)
    # columns = event_df['sample'].str.rsplit('_', 1).str
    # event_df.insert(0, 'cell', columns[1])
    # event_df.insert(0, 'flower', columns[0])
    # event_df.drop('sample', axis=1, inplace=True)

    group_columns = 'feature_1 before_1 after_1 feature_2 before_2 after_2'.split()
    # Instead of event_times[key] we can also use event_df_group.get_group(key)
    # However, this might raise a KeyError.
    # Also, this will return the full DataFrame instead of just the sample, dt, r1 and r2.
    # Should be more flexible though if you'd like to to some post processing on the flower id...
    event_df_grouped = event_df.groupby(group_columns)
    keys, counts = np.unique(event_df.loc[:, group_columns].values, axis=0, return_counts=True)
    tuple_keys = ((tuple(i[:3]), tuple(i[3:])) for i in keys)
    event_counts = collections.Counter(dict(zip(tuple_keys, counts)))

    return all_events, event_times, event_df, event_counts, event_df_grouped


def get_hallmark_timelines(all_data, hallmark_df):
    """Get timelines and times between hallmarks"""
    # Check for any -1 rows.
    ignore_rows = np.all(hallmark_df == -1, axis=1)

    states = [tuple(i[~ignore_rows]) for i in hallmark_df.values.T]
    named_hallmarks = dict(zip(states, hallmark_df.columns))

    timelines = collections.defaultdict(list)
    transitions = []
    # hallmark_counts = collections.Counter({k: 0 for k in named_hallmarks.values()})
    for name, df in all_data.items():
        last = 'start'
        last_t = 0
        start = True
        timelines[name].append((name, 't_start', df.index[0].total_seconds()/60))
        for t, *state in df.dropna(how='any').itertuples():
            state = tuple(int(j) for i, j in enumerate(state) if not ignore_rows[i])
            if state in named_hallmarks:
                new = named_hallmarks[state]
                new_t = t.total_seconds()/60
                # If we just started, we don't want this as a timing, since it doesn't have a
                # proper starting point.
                if start is True:
                    start = new
                    # last = new
                    # last_t = new_t
                if new != last:
                    if start is not True and start != last and last != 'start':
                        transitions.append((name, last, new, new_t - last_t, True))
                    else:
                        transitions.append((name, last, new, new_t - last_t, False))
                    timelines[name].append((new, new_t))
                    # hallmark_counts.update([new])
                    last = new
                    last_t = new_t

        transitions.append((name, last, 'end', df.index[-1].total_seconds()/60 - last_t, False))
        timelines[name].append(('t_end', df.index[-1].total_seconds()/60))

    named_hallmark_transition_df = pd.DataFrame(transitions,
                                                columns=['sample', 'from', 'to',
                                                         'time(min)', 'valid'])
    return timelines, named_hallmark_transition_df


def get_all_transitions(all_data, fillna=False, sentinel=(-1, -1, (-1, -1, -1, -1, -1, -1))):
    """Get all transitions in the dataset."""
    # TODO: Refactor this and get_hallmark_timelines together.
    transitions = []
    sentinel_t, sentinel_nan, sentinel_state = sentinel
    for name, df in all_data.items():
        # Use sentinel start state and time to mark that this is the first data point.
        last_state = sentinel_state
        last_t = sentinel_t
        # To do comparisons, we need to replace NaN with a sentinel value (-1).
        if fillna:
            if fillna is True:
                fillna = 'ffill'
                limit = None
            elif isinstance(fillna, (tuple, list)) and len(fillna) == 2:
                fillna, limit = fillna
            elif fillna in {'backfill', 'bfill', 'pad', 'ffill', None}:
                limit = None
            else:
                raise ValueError("Cannot interpret fillna argument: {}".format(fillna))
            df = df.fillna(method=fillna, limit=limit)
        for t, *state in df.fillna(sentinel_nan).astype(int).itertuples():
            state = tuple(state)
            if state != last_state:
                has_nan = sentinel_nan not in state and sentinel_nan not in last_state
                transitions.append((name, last_t, t, last_state, state, has_nan))
            last_state = state
            last_t = t
        # Use sentinel start state and time to mark that this is the final data point.
        transitions.append((name, t, sentinel_t, state, sentinel_state, False))
    df = pd.DataFrame(transitions, columns=['sample', 'time_before', 'time_after',
                                            'state_before', 'state_after', 'valid'])
    return df


@functools.lru_cache(maxsize=None, typed=False)
def generate_neighbours(state, n_features=6):
    """Generate neighbours for state.

    This generates all neighbours that are +/- 1 for up to n_features states.

    Note: Includes itself.
    """
    neighbours = set()
    # Hack to deal with single feature states.
    if isinstance(state, numbers.Integral):
        state = (state,)
    neigbour_indeces = itertools.chain(*(itertools.combinations(range(-len(state), len(state)),
                                                                r=i)
                                         for i in range(1,  1 + n_features)))
    for indeces in neigbour_indeces:
        s = np.array(state)
        s[list(i for i in indeces if i >= 0)] += 1
        s[list(i for i in indeces if i < 0)] -= 1
        neighbours.add(tuple(s))

    return list(neighbours)


def z_score(state, neighbours):
    """Generate a score for a state compared to it's neighbours."""
    try:
        return (state - neighbours.mean()) / neighbours.std()
    except ZeroDivisionError:
        return np.NaN


def n_neighbours_score(state, neighbours):
    return len(neighbours)


def n_neighbours_score_weighted(state, neighbours):
    return len(neighbours) * neighbours.mean()


def log_z_score(state, neighbours):
    """Generate a score for a state compared to it's neighbours."""
    state = np.log(state)
    neighbours = np.log(neighbours)
    return (state - neighbours.mean()) / neighbours.std()


def fold_score(state, neighbours):
    """Generate a score for a state compared to it's neighbours."""
    return np.log(state / neighbours.mean())


def score_hallmarks(state_count_df, neighbour_function, score_function):
    """Score states of hallmark potential.

    score_function is used to score an individual count vs. the counts of their neighbours,
    as defined by the neighbour function."""
    scores = []
    for state, count in state_count_df['count'].iteritems():
        neighbours = neighbour_function(state)
        # Special case for single identifier state
        if len(neighbours[0]) == 1:
            neighbours = list(itertools.chain.from_iterable(neighbours))
        neighbours = state_count_df.index.intersection(neighbours)
        neighbour_counts = state_count_df.loc[neighbours, 'count']
        neighbour_counts = neighbour_counts[~pd.isna(neighbour_counts.values)]
        scores.append(score_function(count, neighbour_counts))

    return state_count_df.assign(score=scores)


def transition_states(state_from, state_to):
    """Generate all valid transitions states between two states."""
    state_from, state_to = np.array(state_from), np.array(state_to)
    # Ignore negative state differences (regressions) by clipping to 0.
    transitions = (state_to - state_from).clip(0, None)
    idx = np.nonzero(transitions)[0]
    times = transitions[idx]
    indeces = [idx[j] for j in range(len(times))
               for i in range(times[j])]

    # There will be duplicates here, so make a set.
    # Could be made more efficient if required
    combinations = set(itertools.chain(*[itertools.combinations(indeces, i)
                                       for i in range(1, len(indeces))]))

    transition_states = []
    for comb in combinations:
        new_state = state_from.copy()
        for i in comb:
            new_state[i] += 1
        transition_states.append(tuple(new_state))
    return transition_states


def get_state_hallmarks(state_df, hallmark_df):
    """Assign every state in the index of state_df with a hallmark.

    If a row should be ignored in the hallmarks, set the complete row to -1 and it will
    be dropped from both the state_df and the hallmark_df when assigning hallmarks
    """
    ignore = np.any(hallmark_df.values == -1, axis=1)
    hallmark_df = hallmark_df[~ignore]

    # Create mapping of states to hallmarks based.
    bins = {}
    transitions = np.diff(hallmark_df)
    # Note that hallmark here is the column name!
    for i, hallmark in enumerate(hallmark_df):
        state = hallmark_df[hallmark]
        bins[tuple(state)] = hallmark
        try:
            ds = transitions[:, i]
            next_hallmark = hallmark_df.columns[i+1]
        except IndexError:
            continue
        for state in transition_states(state, state+ds):
            if state in bins:
                print("Dual classification {}: {} replaced with {} -> {}".
                      format(state, bins[state], hallmark, next_hallmark))
            bins[state] = "{} -> {}".format(hallmark, next_hallmark)

    hallmarks = []
    for state in np.array(list(state_df.index)):
        # Hack to deal with states defined by single feature.
        if isinstance(state, numbers.Integral):
            hallmarks.append(bins.get((state,), ''))
        else:
            hallmarks.append(bins.get(tuple(state[~ignore]), ''))
    return hallmarks


def permutation_correlation_test(x, y, n_permutations=1000, test=scipy.stats.pearsonr,
                                 ci=.95, alternative='two-sided'):
    x_permuted = x.copy()
    org_r, org_p = test(x, y)
    sample_r = []
    for i in range(n_permutations):
        np.random.shuffle(x_permuted)
        r, p = test(x_permuted, y)
        sample_r.append(r)

    sample_r = np.array(sample_r)

    if alternative == 'two-sided':
        p = (np.abs(sample_r) >= np.abs(org_r)).sum() / n_permutations
    elif alternative == 'greater':
        p = (sample_r >= org_r).sum() / n_permutations
    elif alternative == 'less':
        p = (sample_r <= org_r).sum() / n_permutations
    else:
        raise ValueError("Invalid value for 'alternative': {}".format(alternative))

    # See https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
    z = scipy.stats.norm.ppf(1 - (1 - ci) / 2)
    p_ci = z * np.sqrt((p * (1 - p)) / n_permutations)
    return org_r, org_p, p, p_ci


def check_duration_correlations(dataframes, min_size=5, n_permutations=1000):
    dfs = dataframes
    # Create joined versions of all combinations
    combined_frames = {}
    correlations = []
    for (state1, sample1), (state2, sample2) in itertools.combinations(dfs, 2):
        # The inner join will only keep the variables where the index (flower + cell) matches
        # for both DataFrames.
        combined = sample1.join(sample2, lsuffix='_l', rsuffix='_r', how='inner')

        # Only process further if big enough, or it will never give us anything significant.
        if len(combined) > min_size:
            # Save for checking later
            combined_frames[state1, state2] = combined
            r, p, p_perm, p_ci = permutation_correlation_test(combined['time_l'].values,
                                                              combined['time_r'].values,
                                                              n_permutations)
            correlations.append((state1, state2, len(combined), r, p, p_perm, p_ci))

    state_duration_correlation_df = pd.DataFrame(correlations)
    state_duration_correlation_df.columns = ['state_1', 'state_2', 'n', 'correlation',
                                             'p-value', 'p-value_perm', 'p-value_perm_ci_95']
    return state_duration_correlation_df, combined_frames


def ffillna_limited(df, limit='10M'):
    """Forward fill NaN values up to a maximum time limit of the NaN gap.

    Returns a copy of the original frame."""
    df = df.copy()
    limit = pd.Timedelta(limit)
    # First pass to check the length of NaN gaps and mark for filling
    # Find all NaN values and mark False by default
    na_df = pd.isna(df)
    to_ffill = na_df & False
    # For each column, if the size of a gap is less then limit, set to True.
    for column in range(len(df.columns)):
        c = na_df.iloc[:, column]
        locations = list(np.nonzero(np.diff(np.hstack([c.values, [False]])))[0])
        # If the first row is a NaN, insert it manually so that the diff matches
        # the order of uneven values -> NaN starts, uneven values -> NaN series ends
        if c[0]:
            locations = [0] + locations
        for start, end in zip(locations[::2], locations[1::2]):
            # We can't ffill from the start anyway so skip this.
            if start == 0:
                continue
            # Get time between start and end of the NaN series.
            t = df.index[end] - df.index[start - 1]
            if t <= limit:
                to_ffill.iloc[start:end + 1, column] = True

    # Forward fill for only the marked values.
    ffilled = df.ffill()
    df[to_ffill] = ffilled[to_ffill]
    return df


def bootstrap_hallmark_score(df, method, selection, factor=1.5, n=1000, seed=None):
    score_name, neighbours_f, score_f = method
    # If seed it set, prepare a custom random number generator only
    # for this function.
    rand = np.random.RandomState(seed)
    # Clean data from NaN values
    nans = np.any(pd.isna(df), axis=1)
    df = df[~nans]

    if selection == 'cell':
        joined = df['cell'].astype(str).str.cat(df['flower'].astype(str), '_')
        cells = np.unique(joined)
    elif selection == 'flower':
        flowers = np.unique(df['flower'])

    result = []
    for i in range(n):
        # Take superset or subset
        if selection == 'random':
            sample = rand.choice(len(df), int(len(df)*factor))
            sample = df.iloc[sample]
        elif selection == 'cell':
            sample = rand.choice(len(cells), int(len(cells)*factor))
            sample = pd.concat([df.loc[(joined == c).values]
                                for c in cells[sample]])
        elif selection == 'flower':
            sample = rand.choice(len(flowers), int(len(flowers)*factor))
            sample = pd.concat([df[df['flower'] == f]
                               for f in flowers[sample]])

        # Throw away unneeded columns
        sample = sample.iloc[:, 3:].astype(int)

        # Get counts
        sample_count = pd.DataFrame(sample.groupby(list(sample.columns)).size())
        sample_count.columns = pd.Index(['count'])

        # Score according to method
        score = score_hallmarks(sample_count, neighbours_f, score_f)['score']

        # Save the scores
        result.append(score)

    return pd.DataFrame(result)

if __name__ == '__main__':
    # ------------------------------------------------------------------------------------------ #
    # Script settings
    # ------------------------------------------------------------------------------------------ #
    max_step = 15
    events_fill_na = True  # Forward fill NaN values to detect more events.
    time_resample_interval = '10T'  # 10 minutes
    resample_interpolate_function = lambda x: x.ffill()  # or .nearest / .bfill
    transition_interpolate = None  # 'ffill'
    counts_as_percentage = True
    add_start_end_hallmark = True  # Add the start and end hallmarks by default.
    n_permutations = 10000
    n_hallmark_stages = 11  # Amount of Hallmarks to add, excluding optional start/end.
    min_correlation_size = 5
    hallmark_plot_max_interval_times = ['0M', '30M', '60M', '1200M']

    dropcolumns = [
        # 'cell shape',
        # 'nucleus positoin',
        # 'nucleolus position',
        # 'REC8/Chromatin',
        # 'MT array',
        # 'Tapetum',
    ]
    heatmap_plot_order = ['cell shape', 'MT array', 'nucleus position',
                          'nucleolus position', 'REC8/Chromatin', 'Tapetum']

    # What should be plotted
    FIG_DPI = 300
    plot_series = False
    plot_hists = False
    plot_feature_chains = False
    plot_co_ocurrence = True
    plot_hallmark_scores = False # Note: Hallmark / landmark are used interchangeably.
    plot_hallmark_graph = False
    saveplots = True

    # Do the bootstrap analysis? (Takes some extra time.)
    bootstrap = False

    # Save results to tables?
    save_co_occurence_sheets = False
    save_event_info = False
    save_named_hallmark_info = False
    save_hallmark_sheets = False
    # Note that these results should not be misinterpreted!
    # There is a high correlation to the individuals samples here that is unaccounted for.
    save_state_duration_correlation = False
    save_hallmark_duration_correlation = False

    # Output files
    if saveplots:
        directories = [
            'results',
            'results/figures',
            'results/figures/co-occurences',
            'results/figures/hallmarks',
            'results/figures/timeseries',
            'results/figures/event_hist',
            'results/figures/chains',
        ]
        for d in directories:
            try:
                os.mkdir(d)
            except FileExistsError:
                pass

    if any([save_co_occurence_sheets, save_hallmark_sheets, save_state_duration_correlation,
            save_event_info, save_named_hallmark_info, save_hallmark_duration_correlation]):
        results_writer = pd.ExcelWriter('./results/result_tables.xlsx')

    if bootstrap:
        results_writer_bootstrap = pd.ExcelWriter('./results_april_25_2019/bootstrap_wt.xlsx')

    # Input files
    data_path = glob.glob('data/original/Data extraction_hallmark*.xlsx')

    # Capitalize title columns to match
    # dropcolumns = [i.title() for i in dropcolumns]

    mapping = {}
    invalid = {'n', 'nn', '3or5', '3 or 5'}

    # ------------------------------------------------------------------------------------------ #
    # Input files
    # ------------------------------------------------------------------------------------------ #

    # Load all excel files.
    all_datafiles = [pd.io.excel.ExcelFile(i) for i in data_path]

    # Take the legend from the first file.
    legend_data = all_datafiles[0]

    # Check for duplicate names other than the legend.
    for file_0, file_1 in itertools.permutations(all_datafiles, 2):
        assert set(file_0.sheet_names).intersection(file_1.sheet_names) == set(['legend'])

    # ------------------------------------------------------------------------------------------ #
    # Read all data files
    # ------------------------------------------------------------------------------------------ #

    legend_df, short_legend_df = read_legend(legend_data, dropcolumns=dropcolumns)
    # Read stages/hallmarks and their transitions according to the initial scheme
    hallmark_df, hallmark_transition_df = read_hallmarks(legend_data, dropcolumns=dropcolumns)

    # Load all time line data into a dictionary of sample:DataFrames
    dropcolumns = [i.title() for i in dropcolumns]
    if 'Nucleus Positoin' in dropcolumns:
        dropcolumns[dropcolumns.index('Nucleus Positoin')] = 'Nucleus Position'
    all_data = read_timeseries_data(all_datafiles, dropcolumns=dropcolumns,
                                    max_step=max_step, mapping=mapping, invalid_entries=invalid)
    # ------------------------------------------------------------------------------------------ #
    # Process data into (more usable) formats
    # ------------------------------------------------------------------------------------------ #

    # Find all events in the complete data set
    temp = find_events(all_data, events_fill_na=events_fill_na)
    all_events, event_times, event_df, event_counts, event_df_grouped = temp

    event_data = []
    for key, value in all_events.items():
        df = value.copy().reset_index()
        df.insert(0, 'sample', key)
        event_data.append(df)

    all_events_df = split_sample_id(pd.concat(event_data, ignore_index=True))
    max_valid_range = 15
    valid_events = all_events_df[all_events_df.time_range <= max_valid_range]
    simultaneous_event_counts = (valid_events
                                 # First groupby to get the number of events
                                 # occurring at the same time-point
                                 .groupby(['flower', 'cell', 'time'])
                                 .size().reset_index(name='counts')
                                 # Second groupby to get the total for each of the counts
                                 .groupby('counts').size()).to_frame()
    simultaneous_event_counts.columns = ['counts']
    simultaneous_event_counts['percentage'] = (simultaneous_event_counts['counts'] /
                                               len(valid_events) * 100)

    # Count co-occurring states in the complete dataset
    temp = count_co_occurences(all_data, time_resample_interval=time_resample_interval,
                               resample_interpolate_function=resample_interpolate_function)
    all_df, combination_per_sample_df, combination_df = temp

    # Numbers for paper
    n_cells = len(np.unique(np.array(all_df[['cell', 'flower']].values, dtype=str), axis=0))
    n_samples = len(np.unique(all_df.flower))
    total_time = np.array([i.index[-1] for i in all_data.values()]).sum()
    n_hours = total_time.days * 24 + total_time.seconds // 3600
    n_all = sum(len(i) for i in all_data.values())
    n_all_nan = sum([np.all(pd.isna(i), axis=1).sum() for i in all_data.values()])
    n_some_nan = sum([np.any(pd.isna(i), axis=1).sum() for i in all_data.values()])

    # Co-occurrences
    unstacked = combination_df.unstack(level=[2, 3]).fillna(0).astype(int)
    unstacked.columns = unstacked.columns.droplevel(0)
    unstacked.sort_index(axis=0, inplace=True)
    unstacked.sort_index(axis=1, inplace=True)

    cumulative_group_indeces = np.nonzero(np.diff(unstacked.index.get_level_values(0)))[0]

    simple_index = np.array([short_legend_df.iloc[i, j-1] for i, j in unstacked.index.tolist()])
    # ND is repeated a few times, so replace with unique versions
    # Same with Ce, T and No.
    # Note that this will work but not display the correct one depending on which colums are dropped.
    simple_index = simple_index.astype('<U20')
    n = sum(simple_index == 'ND')
    if n > 1:
        simple_index[simple_index == 'ND'] = ["ND_no_1", "ND_no_2", "ND_rec_fluor", "ND_rec_vis"][:n]
    n = sum(simple_index == 'T')
    if n > 1:
        simple_index[simple_index == 'T'] = ["T_cs_tetr", "T_ma_tetr"][:n]
    n = sum(simple_index == 'No')
    if n > 1:
        simple_index[simple_index == 'No'] = ["No_nu_nn_1", "No_nu_nn_2"][:n]
    n = sum(simple_index == 'Ce')
    if n > 1:
        simple_index[simple_index == 'Ce'] = ["Ce_nc_1", "Ce_nc_2"][:n]

    # Do some work on the co-occurrence index to get it to print nicer.
    labels = np.arange(len(unstacked.index.codes[1]))
    unstacked.index.set_levels([short_legend_df.index.tolist(), simple_index], inplace=True)
    unstacked.columns.set_levels([short_legend_df.index.tolist(), simple_index], inplace=True)
    unstacked.index.set_codes(labels, level=1, inplace=True)
    unstacked.columns.set_codes(labels, level=1, inplace=True)

    percentage_df = (unstacked / unstacked.values.diagonal()) * 100

    # Find Hallmarks
    hallmark_methods = [
                        # ('score_count', lambda x: [x], lambda x, y: x),
                        # ('score_z_r1', lambda x: generate_neighbours(x, 1), z_score),
                        ('score_z_r2', lambda x: generate_neighbours(x, 2), z_score),
                        # ('score_z_r3', lambda x: generate_neighbours(x, 3), z_score),
                        ]

    # Optional bootstrap analysis and relevant settings.
    # Can be resampled randomly perobservation, per cell or per flower.
    if bootstrap:
        import time
        factor = 1.5
        seed = None
        n = 1000
        for method in hallmark_methods:
            t0 = time.time()
            r_random = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'random',
                                                             factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            t0 = time.time()
            r_cell = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'cell',
                                                           factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            t0 = time.time()
            r_flower = pd.DataFrame(bootstrap_hallmark_score(all_df, method, 'flower',
                                                             factor=factor, seed=seed, n=n))
            print(time.time() - t0)

            sheets = ['random', 'cell', 'flower']
            data = [r_random, r_cell, r_flower]

            index_order = [i.title() for i in heatmap_plot_order if i.title() in data[0].columns.names]
            for df, sheet_name in zip(data, sheets):
                sheet_name = method[0] + '__' + sheet_name
                score = df.describe().T.sort_values(by='mean', ascending=False)
                score.index = score.index.reorder_levels(index_order)
                score.to_excel(results_writer_bootstrap, sheet_name=sheet_name)
                # Fill NaN values so they rank last but the total rank count stays the same
                rank = (df.fillna(-2).rank(1, ascending=False)
                        .describe().T.sort_values(by='mean', ascending=True))
                start = len(score.columns) + len(score.index.levels) + 1
                # Shuffle index around to map to figures.
                rank.index = rank.index.reorder_levels(index_order)
                rank.to_excel(results_writer_bootstrap, sheet_name=sheet_name, startcol=start)
        results_writer_bootstrap.save()

    # Remove time, flower and cell columns and all states with one or more NaNs
    all_df_cleaned = all_df.iloc[:, 3:]
    nans = np.any(pd.isna(all_df_cleaned), axis=1)
    all_df_cleaned = all_df_cleaned[~nans].astype(int)

    state_count_df = pd.DataFrame(all_df_cleaned.groupby(list(all_df_cleaned.columns)).size())
    state_count_df.columns = pd.Index(['count'])
    percentage = state_count_df['count'] / state_count_df['count'].sum() * 100
    state_count_df['percentage'] = percentage

    new_hallmarks = []
    new_hallmark_names = []
    for (name, neighbours_f, score_f) in hallmark_methods:
        # Calculate scores based on this measure
        score = score_hallmarks(state_count_df, neighbours_f, score_f)['score']
        # Save in the general DataFrame
        state_count_df = state_count_df.assign(**{name: score})
        # Create a state table based on the score
        if add_start_end_hallmark:
            add_stages = {tuple(i) for _, *i in hallmark_df.iloc[:, [0, -1]].T.itertuples()}
            score = score[[i not in add_stages for i in score.index.values]]
        new_stages = score.nlargest(n_hallmark_stages).index.to_frame()
        new_stages = new_stages.T
        new_stages.columns = range(min(len(new_stages.columns), n_hallmark_stages))
        # To have the same format as the hallmarks DataFrame
        mapping = {'Cell Shape': 'cell shape',
                   'Mt Array': 'MT array',
                   'Nucleolus Position': 'nucleolus position',
                   'Nucleus Position': 'nucleus positoin',
                   'Rec8/Chromatin': 'REC8/Chromatin',
                   'Tapetum': 'Tapetum',
                   }
        new_stages.index = [mapping[i] for i in new_stages.index]
        # Correct order to hallmark equivalent
        new_stages = new_stages.loc[hallmark_df.index.tolist()]
        # Add start and end states if required.
        if add_start_end_hallmark:
            a, b = add_stages
            new_stages = new_stages.assign(a=a, b=b)
        # Sort the stages
        new_stages = new_stages.sort_values(by=new_stages.index.tolist(), axis=1)
        # Assign Hallmark names
        # r = range(n_hallmark_stages + (len(add_stages) if add_start_end_hallmark else 0))
        new_stages.columns = ["H{}".format(i) for i in range(len(new_stages.columns))]
        new_stages.index.name = 'hm_{}'.format(name)
        new_hallmarks.append(new_stages)
        new_hallmark_names.append(new_stages.index.name)

    hallmarks = [hallmark_df] + new_hallmarks
    hallmark_names = ['original'] + new_hallmark_names

    for name, df in zip(hallmark_names, hallmarks):
        column = get_state_hallmarks(state_count_df, df)
        state_count_df = state_count_df.assign(**{name: column})

    # Check correlations of duration of feature state combinations.

    # Only use events that mark the time spent in 1 single feature state
    times = event_df.query('(feature_1 == feature_2) '
                           'and ((before_2 - before_1) == 1) '
                           'and ((after_2 - after_1) == 1)')

    # Pre-process all groups, where each group is the timings of single feature state.
    dfs = []
    for state, df in times.groupby(event_df.columns[5:].tolist()):
        state = state[0], state[2]
        # Can we use the error somehow?
        # dfs.append((state, df.assign(error=df[['error_1', 'error_2']].sum(axis=1))
        #                    .set_index(["flower", "cell"])
        #                    .loc[:, ['time', 'error']]))
        dfs.append((state, df.set_index(["flower", "cell"])
                             .loc[:, ['time']]))

    if save_state_duration_correlation:
        result = check_duration_correlations(dfs, min_size=min_correlation_size,
                                             n_permutations=n_permutations)
        state_duration_correlation_df, combined_state_frames = result

    if save_hallmark_duration_correlation or save_named_hallmark_info:
        # Check the timeline and durations of stages/hallmarks in the complete dataset
        hallmark_transitions = []
        hallmark_correlations = []
        hallmark_combined_frames = []

        for name, df in zip(hallmark_names, hallmarks):
            timelines, named_hallmark_transition_df = get_hallmark_timelines(all_data, df)
            ordering = dict((j, i) for i, j in enumerate(df.columns.tolist()))

            hallmark_transitions.append(named_hallmark_transition_df)

            if save_hallmark_duration_correlation:
                df = named_hallmark_transition_df.copy()
                # Split sample column into flower and cell
                columns = df['sample'].str.rsplit('_', 1).str
                df.insert(0, 'cell', columns[1])
                df.insert(0, 'flower', columns[0])
                df.drop('sample', axis=1, inplace=True)
                # Drop invalid columns
                df = df[df.valid].drop('valid', axis=1)

                dfs = []
                for (from_state, to_state), df in df.groupby(['from', 'to']):
                    if ordering[from_state] == ordering[to_state] - 1:
                        dfs.append((from_state, df.set_index(["flower", "cell"])
                                                .rename({'time(min)': 'time'}, axis=1)
                                                .loc[:, ['time']]))
                if dfs:
                    result = check_duration_correlations(dfs, min_size=min_correlation_size,
                                                        n_permutations=n_permutations)
                else:
                    result = pd.DataFrame(), []

                hallmark_duration_correlation_df, combined_hallmark_frames = result

                hallmark_correlations.append(hallmark_duration_correlation_df)
                hallmark_combined_frames.append(combined_hallmark_frames)

    # ------------------------------------------------------------------------------------------ #
    # Section for excel output tables.
    # ------------------------------------------------------------------------------------------ #

    if save_event_info:
        df = event_df_grouped['time'].describe()
        df.to_excel(results_writer, sheet_name='pairwise_event_info',
                    merge_cells=True)
        df_sorted = df.sort_values(by='count', ascending=False)
        df_sorted.to_excel(results_writer, sheet_name='pairwise_event_info_by_count',
                           merge_cells=False)
        results_writer.save()

    if save_named_hallmark_info:
        for i, (name, df) in enumerate(zip(hallmark_names, hallmark_transitions)):
            df = df.groupby(['from', 'to']).describe()
            df.index.name = name
            spacing = 1 + len(df.columns) + len(df.index.levels)
            df.to_excel(results_writer, sheet_name='hallmark_info', startcol=i*spacing)

        results_writer.save()

    if save_state_duration_correlation:
        df = state_duration_correlation_df.sort_values(by='p-value_perm')
        df.to_excel(results_writer, sheet_name='state_duration_correlations')
        results_writer.save()

    if save_hallmark_duration_correlation:
        for i, (name, df) in enumerate(zip(hallmark_names, hallmark_correlations)):
            if len(df) == 0:
                continue
            df.index.name = name
            df = df.sort_values(by='p-value_perm')
            spacing = 1 + len(df.columns) + 1
            df.to_excel(results_writer, startcol=i*spacing,
                        sheet_name='hallmark_duration_correlations')
        results_writer.save()

    if save_co_occurence_sheets:
        unstacked.to_excel(results_writer, sheet_name='co-occurrence counts')
        percentage_df.to_excel(results_writer, sheet_name='co-occurrence percentages')
        results_writer.save()

    if save_hallmark_sheets:
        state_count_df.to_excel(results_writer, sheet_name='hallmark_by_hallmark',
                                merge_cells=False)
        state_count_df_sorted = state_count_df.sort_values(by='percentage', ascending=False)
        state_count_df_sorted.to_excel(results_writer, sheet_name='hallmark_by_percentage',
                                       merge_cells=False)

        # Calculate the coverage of each of the hallmark schemes and print to a single sheet.
        for i, name in enumerate(hallmark_names):
            df = state_count_df.groupby(name)['percentage'].sum()
            # In get_state_hallmarks we used the -> arrow to note stages classified as transitions.
            transition_stages = df.index.str.contains('->')
            transition_sum = df[transition_stages].sum()
            try:
                not_assigned = df.loc['']
            except KeyError:
                not_assigned = 0
            hallmarks_sum = df[~transition_stages].sum() - not_assigned
            index = ['total assigned', 'all hallmarks',
                     'all transitions', 'total unassigned'] + df.index.tolist()[1:]
            values = df.tolist()
            values = [hallmarks_sum + transition_sum,
                      hallmarks_sum, transition_sum] + values
            df = pd.DataFrame(data=values, index=index)
            df.columns = ['percentage']
            # Move H1x to the end to maintain the logical ordering of hallmark stages.
            to_move = df.index.str.match('H\d\d+')
            df = pd.concat([df[~to_move], df[to_move]])
            df.index.name = name
            spacing = 2 + len(df.columns)
            df.to_excel(results_writer, sheet_name='hallmark_tables', startcol=i*spacing)

        results_writer.save()

    # ------------------------------------------------------------------------------------------ #
    # Section for plotting
    # ------------------------------------------------------------------------------------------ #

    if plot_feature_chains:
        for c, feature in enumerate(legend_df.index):
            length = (~pd.isna(legend_df.loc[feature])).sum()
            r = range(1, (~pd.isna(legend_df.loc[feature])).sum() - 1)
            chain = [((c, i, i+1), (c, i+1, i+2)) for i in r]
            plot_event_hist_chain(event_times, chain, save=saveplots)

    # Plot data in nice timeline format
    if plot_series:
        for name, data in all_data.items():
            plot_time_series(data, name, saveplots)

    # Plot the distribution of the most common combinations
    if plot_hists:
        for key, _ in event_counts.most_common(50):
            times = event_times[key] + [(s, -t, e1, e2) for s, t, e1, e2 in event_times[key[::-1]]]
            plot_event_hist(event_times[key], key, legend_df, saveplots, errors=True, nbins=25)

    if plot_co_ocurrence:
        df = percentage_df.rename(index={'nucleus positoin': 'nucleus position'})
        df = df.rename(columns={'nucleus positoin': 'nucleus position'})
        fig, textboxes = plot_heatmap(df, heatmap_plot_order)
        if saveplots:
            fig.savefig('results/figures/co-occurences/heatmap.png',
                        bbox_inches='tight', dpi=FIG_DPI,
                        bbox_extra_artists=textboxes)
            plt.close(fig)

    if plot_hallmark_scores:
        for name, _, _ in hallmark_methods:
            hallmark_scores = state_count_df[name].sort_values(na_position='first')
            ax = hallmark_scores.plot.bar()
            fig = ax.get_figure()
            try:
                labels = [str(tuple(int(i) for i in j))[1:-1] for j in hallmark_scores.index.values]
            except TypeError:
                labels = [str(j) for j in hallmark_scores.index.values]
            ax.set_xticklabels(labels, fontdict={'fontsize': 5})
            ax.set_xlabel('')
            ax.set_ylabel('score')
            ax.set_title('{}'.format(name))
            xmin, xmax = ax.get_xlim()
            ax.hlines([1, 2], xmin, xmax, linestyle='dashed', color='black', alpha=.5)

            sns.despine(fig)
            fig.tight_layout()
            if saveplots:
                fig.savefig('results/figures/hallmarks/hallmark_scores_{}.png'.format(name),
                            bbox_inches='tight', dpi=FIG_DPI)
                plt.close(fig)

    if plot_hallmark_graph:
        import networkx as nx
        for t in hallmark_plot_max_interval_times:
            limited_fill_data = {k: ffillna_limited(v, t) for k, v in all_data.items()}
            all_transitions_df_ffill = get_all_transitions(limited_fill_data, fillna=None)
            transition_counts_ffill = (all_transitions_df_ffill[all_transitions_df_ffill['valid']]
                                       .groupby(['state_before', 'state_after'])['sample'].count())
            # diff = (transition_counts_ffill - transition_counts).replace(0, np.NaN).dropna()

            default_score = 'percentage'
            for name in hallmark_names:
                if 'hm_score' in name:
                    score_column = name[3:]
                else:
                    continue
                    score_column = default_score
                state_counts = state_count_df.loc[:, ['count', score_column, name]]
                plot_transition_hallmark_graph(state_counts, transition_counts_ffill,
                                               score_column=score_column, name=name,
                                               shape='circle', plot_other_nodes=True,
                                               set_colorbar=True, save=saveplots,
                                               rename_start_end=True, invalid_striketrough=False,
                                               fig_size=(12, 12), t_ffill=t, hallmark_initial='A',
                                               dpi=FIG_DPI)

    # ------------------------------------------------------------------------------------------ #
